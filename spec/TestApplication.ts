import { ExpressDataApplication, serviceRouter } from '@themost/express';
import express, { Application } from 'express';
import passport from 'passport';
import { Strategy as BearerStrategy } from 'passport-http-bearer';
import { HttpUnauthorizedError } from '@themost/common';
import { DefaultServerSentEventService, ServerSentEventService } from '../src';

export function getApplication(): Application {

    passport.use(new BearerStrategy((token: string, done: (error: any, user?: any) => void) => {
        if (token == null) {
            return done(new HttpUnauthorizedError());
        }
        return done(null, {
            name: 'registrar1@example.com',
            authenticationProviderKey: 150,
            authenticationScope: 'registrar'
        });
    }));
    const app = express();
    const dataApplication = new ExpressDataApplication();
    app.set(ExpressDataApplication.name, dataApplication);
    app.use(dataApplication.middleware(app));
    dataApplication.useStrategy(ServerSentEventService as any, DefaultServerSentEventService as any);
    app.use('/api', passport.authenticate('bearer', {session: false}), serviceRouter);
    return app;

}