import { DefaultServerSentEventService, ServerSentEventService } from '../src';
import { ExpressDataApplication } from '@themost/express';
import request from 'supertest';
import { getApplication } from './TestApplication';
import { Application } from 'express';
import { RandomUtils } from '@themost/common';
describe('ServerSentEventService', () => {
    let app: ExpressDataApplication;
    let container: Application;
    beforeAll( async () => {
        container = getApplication();
        app = container.get(ExpressDataApplication.name);
    });
    it('should create instance', async () => {
        const service = new DefaultServerSentEventService(app);
        expect(service).toBeTruthy();
        service.finalize();
    });
    it('should get events', async () => {
        const messages = [];
        const response = await request(container).get('/api/users/me/events/subscribe').set({
            'Accept': 'application/json',
            'Authorization': `Bearer ${RandomUtils.randomChars(24)}`
        }).parse((res, callback) => {
            res.on('data', (chunk) => {
                const eventResponse: any = res;
                // tslint:disable-next-line: no-console
                const body = Buffer.from(chunk).toString()
                messages.push(body);
                if (body === 'data: []') {
                    app.getService(ServerSentEventService).send('registrar1@example.com', 'registrar', {
                        model: 'User',
                        target: 100,
                        status: 1
                    });
                    // send event to another user
                    app.getService(ServerSentEventService).send('registrar2@example.com', 'registrar', {
                        model: 'User',
                        target: 100,
                        status: 1
                    });
                } else {
                    eventResponse.req.destroy();
                    // send another message
                    app.getService(ServerSentEventService).send('registrar1@example.com', 'registrar', {
                        model: 'User',
                        target: 100,
                        status: 1
                    });
                    return callback(null, messages);
                }
            });
            res.on('error', (err: any) => {
                return callback(err, null);
            });
          });
        expect(response.status).toBe(200);
        expect(messages && messages.length).toBe(2);
    });

});