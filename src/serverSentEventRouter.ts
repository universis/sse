import { HttpError, HttpForbiddenError, TraceUtils } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';
import express, { NextFunction, Request, Response } from 'express';
import moment from 'moment';
import { ServerSentEventService } from './ServerSentEventService';

export class ServerSideEventUnavailableError extends HttpError {
    constructor() {
        super(503, 'Server-side event service is unavailable');
    }
}

export function serverSentEventRouter(app: ExpressDataApplication) {
    const router  = express.Router();
    router.get('/subscribe', (req: Request, res: Response, next: NextFunction) => {
        try {
            const service = req.context.application.getService(ServerSentEventService);
            if (service == null) {
                return next(new ServerSideEventUnavailableError());
            }
            const headers = {
                'Content-Type': 'text/event-stream',
                'Connection': 'keep-alive',
                'Cache-Control': 'no-store',
                'X-Accel-Buffering': 'no'
              };
            if (req.context.user == null) {
                return next(new HttpForbiddenError());
            }
            // get expiration timeout in seconds
            const expiration = app.getConfiguration().getSourceAt('settings/universis/sse/expirationTimeout') || 120;
            // set created at
            const createdAt = new Date();
            // set expires at
            const expiresAt = moment(createdAt).add(expiration, 's').toDate();
            // get user and scope to build key
            const user = req.context.user?.name;
            let scope = req.context.user.authenticationScope || 'profile';
            // if query contains scope
            if (typeof req.query.scope === 'string') {
                // get scopes as array
                const scopes = scope.split(',').map((value) => value.trim());
                // try to find scope
                if (scopes.indexOf(req.query.scope) > -1) {
                    // if scopes contains the given scope, use this scope and continue
                    scope = req.query.scope;
                }
            }
            // set client
            service.clients.set(`${user}:${scope}`, {
                user,
                createdAt,
                expiresAt,
                response: res,
                scope
            });
            req.context.finalize(() => {
                // send data
                res.writeHead(200, headers);
                res.write(`data: ${JSON.stringify(null)}`);
                res.write('\n\n');
                TraceUtils.debug(`ServerSentEventService: ${user}:${scope}, open connection`);
                // handle response close
                res.on('close', () => {
                    const request = res.req;
                    if (request && request.context && request.context.user != null) {
                        TraceUtils.debug(`ServerSentEventService: ${user}:${scope}, connection closed`);
                        service.clients.delete(`${user}:${scope}`);
                    }
                });
            });
        } catch (err) {
            return next(err);
        }
    });
    return router;
}