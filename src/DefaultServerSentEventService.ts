import { ApplicationService, TraceUtils } from '@themost/common';
import { serviceRouter } from '@themost/express';
import { Application } from 'express';
import { serverSentEventRouter } from './serverSentEventRouter';
import { ServerSentEventClient, ServerSentEventServiceBase, ServerSentEventClientCollection } from './ServerSentEventService';
import { ServerSentEvent } from './ServerSentEventService';
import { InterProcessEventEmitter } from '@universis/racket';

const instances: DefaultServerSentEventService[] = [];

declare interface ServerSentEventMessage {
    type: 'ServerSentEvent' | string, 
    message?: { 
        user: string,
        scope: string,
        event: any
    }
}

class DefaultServerSentEventService extends ApplicationService implements ServerSentEventServiceBase {
    private _clients: ServerSentEventClientCollection;
    private _handle: any;
    private _emitter: InterProcessEventEmitter<ServerSentEventMessage>;

    constructor(app: any) {
        super(app);
        this._clients = new ServerSentEventClientCollection();
        if (app && app.container) {
            app.container.subscribe((container: Application) => {
                if (container != null) {
                    serviceRouter.use('/users/me/events', serverSentEventRouter(app));
                }
            });
        }
        this._handle = setInterval(() => {
            const time = new Date();
            for (const [k, value] of this.clients) {
                if (value.expiresAt < time) {
                    if (value.response.destroyed === false) {
                        value.response.end();
                    }
                }
              }
        }, 30000);
        instances.push(this);
        TraceUtils.log('ServerEventService: Service instance is going to activate inter-process event subscriptions');
        this._emitter = new InterProcessEventEmitter<ServerSentEventMessage>();
        this._emitter.subscribe((event: ServerSentEventMessage) => {
            if (event && event.type === 'ServerSentEvent') {
                // get value
                if (event.message && event.message) {
                    this.next(event.message.user, event.message.scope, event.message.event);
                }
            }
        });
    }

    finalize() {
        if (this._handle) {
            clearInterval(this._handle)
        }
        if (this._emitter) {
            this._emitter.unsubscribe();
        }
        for (const [k, value] of this.clients) {
            if (value.response.destroyed === false) {
                value.response.end();
            }
          }
        this.clients.clear();
    }

    emit(user: string, scope: string, event: ServerSentEvent | any): boolean {
        this._emitter.emit({
            type: 'ServerSentEvent',
            message: {
                user,
                scope,
                event
            }
        } as ServerSentEventMessage)
        return true;
    }

    next(user: string, scope: string, event: ServerSentEvent | any): boolean {
        let found: ServerSentEventClient[] = [];
        if (scope === '*') {
            found = this.clients.getUserClients(user)
        } else {
            const client = this.clients.get(`${user}:${scope}`);
            if (client != null)
            found = [
                client
            ]
        }
        if (found.length == 0) {
            return false;
        }
        for (const client of found) {
            if (client != null) {
                try {
                    client.response.write(`data: ${JSON.stringify(event)}`);
                    client.response.write('\n\n');
                } catch (err) {
                    TraceUtils.error('ServerSentEventService', user, scope, 'An error occurred while sending server-side event');
                    TraceUtils.error(err);
                }
            }
        }
        return true;
    }

    public get clients(): ServerSentEventClientCollection {
        return this._clients;
    }

}

export {
    DefaultServerSentEventService
}