import { AbstractMethodError } from '@themost/common';
import { Response } from 'express';
import { SyncSeriesEventEmitter } from '@themost/events'

interface ServerSentEventClientKey {
    user: string;
    scope?: string;
}

interface ServerSentEventClient {
    user: string;
    response: Response;
    createdAt: Date;
    expiresAt: Date;
    scope?: string;
}

interface ServerSentEvent {
    id?: string;
    additionalType?: string;
    entitySet?: string;
    entityType?: string;
    entityAction?: string;
    target?: any;
    result?: any;
    status?: any;
    error?: any;
    recipient?: string;
    scope?: string;
    dateCreated?: Date;
}

interface ServerSentEventServiceBase {
    readonly clients: Map<string, ServerSentEventClient>;
    finalize(): void;
    emit(user: string, scope: string, event: any): boolean;
}

class ServerSentEventClientCollection extends Map<string, ServerSentEventClient> {
    private _indices: Map<string, string[]>;
    public added = new SyncSeriesEventEmitter<{key: string, value:ServerSentEventClient}>();
    public deleted = new SyncSeriesEventEmitter<{key: string, value:ServerSentEventClient}>();
    public purged = new SyncSeriesEventEmitter<void>();
    constructor() {
        super();
        this._indices = new Map();
        this.added.subscribe((event) => {
            const index = this._indices.get(event.value.user);
            if (index == null) {
                this._indices.set(event.value.user, [
                    event.key
                ]);
            } else {
                if (index.indexOf(event.key) < 0) {
                    index.push(event.key);
                }
            }
        });
        this.purged.subscribe(() => {
            this._indices.clear();
        });
        this.deleted.subscribe((event) => {
            const item = this._indices.get(event.value.user);
            if (item) {
                if (item.length === 1) {
                    this._indices.delete(event.value.user);
                    return;
                }
                const deleteIndex = item.indexOf(event.key);
                if (deleteIndex >= 0) {
                    item.splice(deleteIndex, 1);
                }
            }
        });
    }
    
    set(key: string, value: ServerSentEventClient): this {
        super.set(key, value);
        this.added.emit({
            key,
            value
        });
        return this;
    }

    getUserClients(user: string): ServerSentEventClient[] {
        const keys = this._indices.get(user);
        if (keys) {
            return keys.filter((key) => {
                return this.has(key);
            }).map((key) => {
                return this.get(key);
            }) as ServerSentEventClient[];
        }
        return [];
    }

    clear(): void {
        super.clear();
        this.purged.emit();
    }

    delete(key: string): boolean {
        const value = super.get(key);
        if (value) {
            this.deleted.emit({
                key,
                value
            });
        }
        const result = super.delete(key);
        return result;
    }
}

class ServerSentEventService implements ServerSentEventServiceBase {

    constructor() {
        this._clients = new Map();
    }
    /**
     * Finalizes server-sent event service
     */
    finalize(): void {
        throw new AbstractMethodError();
    }
    /**
     * Emits a server-sent event to the specified user by using an application scope.
     * @param {string} user A user who is going to receive the server-sent event
     * @param {string} scope A scope which commonly defines the application where the user is going to receive the event
     * @param {ServerSentEvent|*} event Event data that is going to be sent
     */
    emit(user: string, scope: string, event: ServerSentEvent | any): boolean {
        throw new AbstractMethodError();
    }

    private _clients: Map<string, ServerSentEventClient>;
    private _indices: Map<string, string[]>
    /**
     * The collection of active clients that are receiving server events
     */
    public get clients() {
        return this._clients;
    }

}

export {
    ServerSentEvent,
    ServerSentEventClientKey,
    ServerSentEventClientCollection,
    ServerSentEventClient,
    ServerSentEventServiceBase,
    ServerSentEventService
}