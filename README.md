# @universis/sse

![Universis logo](https://gitlab.com/universis/universis-api/-/raw/master/public/images/universis_196.png)

An implementation of server-sent events for [Universis api server](https://gitlab.com/universis/universis-api) based on [Server-sent events specification](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events)

## Installation

    npm i @universis/sse

## Configuration

Register `ServerSideEventService` under `services` section of application services:

    {
        "services": [
            ...,
            {
                "serviceType": "@universis/sse#ServerSentEventService",
                "strategyType": "@universis/sse#DefaultServerSentEventService"
            }
    ]

Configure `ServerSideEventService` under `settings/universis/sse` section:

    {
        "settings": {
            "universis": {
                ...
                "sse": {
                    "expirationTimeout": 20
                }
            }
        }
    }

The `expirationTimeout` defines the duration of a server-side event client in minutes. The default value is 20 minutes.